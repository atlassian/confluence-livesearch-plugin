package it.webdriver.com.atlassian.confluence.plugins.livesearch.pageobjects;

import com.atlassian.confluence.webdriver.pageobjects.component.ConfluenceAbstractPageComponent;
import com.atlassian.confluence.webdriver.pageobjects.page.SearchResultPage;
import com.atlassian.confluence.webdriver.pageobjects.page.search.SearchPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.atlassian.webdriver.utils.Check.elementIsVisible;
import static java.util.Collections.emptyList;
import static org.hamcrest.CoreMatchers.is;
import static org.openqa.selenium.By.cssSelector;

public class LiveSearchMacro extends ConfluenceAbstractPageComponent {

    @ElementBy(cssSelector = "form.search-macro")
    private PageElement livesearchForm;

    @ElementBy(cssSelector = "div.search-macro-query input")
    private PageElement searchbox;

    @ElementBy(cssSelector = "button.search-macro-button")
    private PageElement searchButton;

    @ElementBy(cssSelector = "div.search-macro-dropdown ol")
    private PageElement resultContent;

    public SearchPage clickSearch() {
        searchButton.click();
        return pageBinder.bind(SearchPage.class);
    }

    public String getSearchPlaceholder() {
        return searchbox.getAttribute("placeholder");
    }

    public Iterable<LiveSearchSearchResult> searchFor(String query) {
        List<PageElement> searchElements = doSearch(query);
        return searchElements.stream().map(pageElement -> {
            LiveSearchSearchResult.Builder builder = LiveSearchSearchResult.newSearchResult();

            builder.setTitle(pageElement.find(cssSelector("a span em")).getText())
                    .setUrl(pageElement.find(By.tagName("a")).getAttribute("href"))
                    .setContentType(pageElement.getAttribute("data-content-type"));

            if (pageElement.hasClass("with-additional"))
                builder.setAdditional(pageElement.find(cssSelector("a.additional")).getText());

            return builder.build();
        }).collect(Collectors.toList());
    }

    private List<PageElement> doSearch(final String query) {
        searchbox.type(query);
        waitUntil(searchbox.timed().getAttribute("data-last-search"), is(query));

        if (isDropDownVisible()) {
            return resultContent.findAll(By.tagName("li"));
        }

        return emptyList();
    }

    public boolean isDropDownVisible() {
        return elementIsVisible(cssSelector("div.search-macro-dropdown"), driver);
    }

    public SearchResultPage clickSearchForMore() {
        PageElement searchFor = pageElementFinder.find(cssSelector("div.search-macro-dropdown .search-for"));
        waitUntilTrue(searchFor.timed().isVisible());
        searchFor.click();
        return pageBinder.bind(SearchResultPage.class);
    }
}
