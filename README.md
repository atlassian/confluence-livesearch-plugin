# THIS REPOSITORY HAS BEEN FROZEN AND MIGRATED

This repository has been migrated to the confluence-public-plugins repository. This is part of the developer productivity team's efforts in bringing homogenous plugins closer together and allowing for overarching changes to be easier to manage. Please checkout the new repository in order to start working on this plugin.

* New home: [confluence-public-plugins](https://bitbucket.org/atlassian/confluence-public-plugins/src/master/) 
* Project status: [Confluence Monorepo Plugin Status](https://hello.atlassian.net/wiki/spaces/CSD/pages/2786862612/Confluence+Monorepo+plugin+migration+status) 
* Project overview: [Move closer to Confluence DC monorepo and colocation of tests](https://hello.atlassian.net/wiki/spaces/CSD/pages/2641350532/Move+closer+to+Confluence+DC+monorepo+and+colocation+of+tests) 
* Contact for help: Contact person on Project status page or [#conf-dc-thunderbird](https://atlassian.slack.com/archives/C01KJLNDB1D)

# README #
Server fork of live search macro - the cloud version is [here](https://stash.atlassian.com/projects/CONFCLOUD/repos/confluence-livesearch-plugin/browse)

### Confluence Live Search Macro ###

* Provides a macro that renders a search box on a page that performs a search as a user types